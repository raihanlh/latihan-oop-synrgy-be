public class Anggota extends ClassBE {
    @Override
    public String m_judul() {
        return "Ini method dari class Anggota";
    }

    @Override
    public String m_anggota(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
//        System.out.println("nama=" + getNama() + " umur=" + getUmur());

        return "nama=" + getNama() + " umur=" + getUmur();
    }

    public String m_anggota(int umur, String nama) {
        this.nama = nama;
        this.umur = umur;
//        System.out.println("nama=" + getNama() + " umur=" + getUmur());

        return "nama=" + getNama() + " umur=" + getUmur();
    }
}
