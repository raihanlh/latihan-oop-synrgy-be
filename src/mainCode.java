public class mainCode {
    public static void main(String[] args) {
        // b
        ClassBE cbe = new ClassBE();
        // c
        cbe.m_judul();
        // d
        System.out.println(cbe.m_anggota("Budi", 20));
        // e
        Anggota agt = new Anggota();
        // f
        System.out.println(agt.m_anggota("Samsul", 18));
        // g
        System.out.println(agt.m_judul());
        // h
        System.out.println(agt.m_anggota(40, "Joko"));
    }
}
